﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scroller : MonoBehaviour
{
    Material material;
    Vector2 offset;

    public float speed;
    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector2(speed * Time.deltaTime, 0);
    }

    // Update is called once per frame
    void Update()
    {
        material.mainTextureOffset += offset;
    }
}
