﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartMenu : MonoBehaviour
{
    //restart current level
   public void restartLevel()
    {
        SceneManager.LoadScene(GameController.gc.getLevel().ToString());
    }

    //go back to main menu
   public void mainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
