﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeController : MonoBehaviour
{
    public float speed = 3f;
    private Rigidbody2D rigidbody2D;
    private float distance;
    private bool mright = true;
    public Transform groundCheck;



    void Start()
    {
        
    }

    private void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        //raycast checks if object will move off platform if continues direction
        RaycastHit2D groundInfo = Physics2D.Raycast(groundCheck.position, Vector2.down, 2f);
        //happens when raycast finds nothing below it
        if (groundInfo.collider == false)
        {
            //changes directions
            if(mright == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                mright = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                mright = true;
            }
        }
    }





}
