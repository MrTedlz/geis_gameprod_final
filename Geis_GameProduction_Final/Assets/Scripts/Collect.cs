﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect : MonoBehaviour
{

    //collects a nut
    void OnTriggerEnter2D(Collider2D other)
    {
        GameController.gc.addNut();
        Destroy(gameObject);

    }
}
