﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour
{
    private void Start()
    {
        print("script runs");
    }
    //when collide with player restart level
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameController.gc.setRestartLevel(true);

        }
        
    }
}
