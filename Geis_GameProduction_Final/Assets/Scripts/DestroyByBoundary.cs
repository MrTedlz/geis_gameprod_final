﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.CompareTag("Storm"))
        {
           
        }
        //restart if player
        else if (collision.CompareTag("Player"))
        {
            GameController.gc.setRestartLevel(true);
        }
        //destroy object if not player(for birds)
        else
        {
            Destroy(collision.gameObject);
        }
    }
}
