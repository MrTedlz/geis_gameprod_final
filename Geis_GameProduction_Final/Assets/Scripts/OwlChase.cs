﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlChase : MonoBehaviour
{
    public GameObject target;
    public float speed = 1f;
    Rigidbody2D rb;
    Vector2 proximity;
    bool locked;

    // Start is called before the first frame update
    void Start()
    {
        target = GameController.gc.playerClone;
        //create target for the bird object
        if (target != null)
        {
            rb = GetComponent<Rigidbody2D>();
            locked = false;
            

            Vector2 direction = target.transform.position - transform.position;
            //rotates the model to fit its direction
            if (direction.x > 0)
            {
                transform.Rotate(0, 90, 0);
            }
            else
            {
                transform.Rotate(0, -90, 0);
            }

            // sets the speed for the birds based on level
            if (GameController.gc.getLevel() == 2)
            {
                speed = 2f;
            }
            else if (GameController.gc.getLevel() == 3)
            {
                speed = 3f;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (target != null)
        {
            //all moves bird towards player
            var dist = Vector2.Distance(target.transform.position, transform.position);
            Vector2 direction = target.transform.position - transform.position;


            //once within a certain distance stop bird from moving towards player
            if (dist < 5)
            {
                locked = true;
            }
            else if (dist > 5 && !locked)
            {
                direction = direction.normalized;
                rb.velocity += speed * direction * Time.deltaTime;
            }
            else
            {

            }
        }
    }
}
