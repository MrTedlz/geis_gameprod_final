﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //start level 1
   public void PlayGame()
    {
        SceneManager.LoadScene("1");
    }
    //exit application
    public void QuitGame()
    {
        Application.Quit();
    }
}
