﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatformController : MonoBehaviour
{
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    [HideInInspector] public bool jumping = false;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;

    public Transform groundCheck;
    public Animator anim;

    private bool grounded = false;

    private Rigidbody2D rb2d;

    private void Awake()
    {

        rb2d = GetComponent<Rigidbody2D>();

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //checks if player is on ground
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        //if character is grounded set gravity back to normal
        if (grounded)
        {
            rb2d.gravityScale = 1f;

        }


        //if character is grounded and jump button is hit, jump the character
        if (grounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
                jumping = true;

            }


        }
        else
        {   //if the character is not grounded and jump is hit/held make the character glide by changing gravity
            if (Input.GetButton("Jump") && rb2d.velocity.y < 0)
            {

                rb2d.gravityScale = 0.2f;
                jumping = true;

            }

        }

        if (!Input.GetButton("Jump"))
        {
            rb2d.gravityScale = 1f;


        }


    }
    private void FixedUpdate()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        float h = Input.GetAxis("Horizontal");
        print(h);

        if (h == 0)
        {
            anim.SetBool("jumping", false);
            anim.SetBool("running", false);
        }


        //cases for allowing player to speed up until the hit max speed
        // *********************************************************
        //if moving and grounded. move and play run animation
        if (h * rb2d.velocity.x < maxSpeed && grounded && h != 0)
        {
            rb2d.AddForce(Vector2.right * h * moveForce);
            anim.SetBool("running", true);



        }
        //if moving and not grounded. move and play/finish jump animation
        else if (h * rb2d.velocity.x < maxSpeed && !grounded)
        {
            rb2d.AddForce(Vector2.right * h * moveForce);
            anim.SetBool("running", false);
        }

        //cases for slowing player down back to max speed
        //***************************************************
        //if moving and grounded. move and play run animation
        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed && grounded)
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            anim.SetBool("running", true);
        }
        //if moving and not grounded. move and play/finish jump animation
        else if (Mathf.Abs(rb2d.velocity.x) > maxSpeed && !grounded)
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            anim.SetBool("running", false);
        }

        //if jumping set animations for jump(cancel run)
        if (jump)
        {
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
            anim.SetBool("jumping", true);
            anim.SetBool("running", false);
        }
        //else make sure jumping param is false
        else
        {
            anim.SetBool("jumping", false);
        }
        //adds jumping animation anytime character is not grounded
        if (!grounded)
        {
            anim.SetBool("jumping", true);
        }

    }
}
    
