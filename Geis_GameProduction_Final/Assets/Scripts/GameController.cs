﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController gc;

    public int level = 1;

    private int stormSpeed;
    private bool restartLevel;
   
    private int maxNuts;
    private int nutCount = 0;
    public GameObject player;
    public GameObject playerClone;
    
    private int maxBirds = 5;
    public float spawnWait;
    public float waveWait;
    public Vector3 spawnValue;
    public Vector3 startPoint;
    public GameObject bird;
    public float startWait;
    public Text scoreText;


    private void Awake()
    {
       
        
    }
    // Start is called before the first frame update
    void Start()
    {
        //allows method to run after scene is loaded
        SceneManager.sceneLoaded += OnSceneLoaded;
        
        //singleton pattern
        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
            
        }
        else
        {
            Destroy(gameObject);
            
            return;
        }
        //spawns birds
        StartCoroutine(spawnWaves());
    }

    // Update is called once per frame
    void Update()
    {
        //sets the score text every frame
        scoreText.text = "Nuts Gathered:" + nutCount + "/" + maxNuts;

        //if any collider action kills player it makes restart level true, which takes the player to the restart scene
        if (restartLevel)
        {
            print(restartLevel);
            restartLevel = false;
            SceneManager.LoadScene("Restart Level");
            
        }
    }

    IEnumerator spawnWaves()
    {
        //allows max birds to change based on level
        if(level == 2)
        {
            maxBirds = 10;
        }
        else if(level == 3)
        {
            maxBirds = 15;
        }
        //gives the player some time before the birds start
        yield return new WaitForSeconds(startWait);
        while (true)
        {
           
            for (int i = 0; i < maxBirds; i++)
            {
                //spawns birds at random start positions and sets their spawn rotation
                Vector3 spawnPos = new Vector3(UnityEngine.Random.Range(-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
                Quaternion spawnRot = Quaternion.identity;
                Instantiate(bird, spawnPos, spawnRot);
                //little bit of time between birds
                yield return new WaitForSeconds(spawnWait);
            }
            //time between full waves
            yield return new WaitForSeconds(waveWait);
            

        }

    }
    //sets restart boolean value
    public void setRestartLevel(bool value)
    {
        restartLevel = value;
    }

    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    //when scene is loaded, re set the game objects that were destroyed on reset game
    void OnSceneLoaded(Scene scene , LoadSceneMode mode)
    {
        playerClone = GameObject.FindGameObjectWithTag("Player");
        scoreText = GameObject.Find("Text").GetComponent<Text>();
        nutCount = 0;
        if (level == 1)
        {
            maxNuts = 3;
        }
        else if (level == 2)
        {
            maxNuts = 10;
        }
        else if (level == 3)
        {
            maxNuts = 15;
        }
    }

    //adds a nut
    public void addNut()
    {
        nutCount += 1;
    }
    
    //access the nut variable
    public int getNutCount()
    {
        return nutCount;
    }

    //increment level
    public void addLevel()
    {
        level += 1;
    }

    //get current level
    public int getLevel()
    {
        return level;
    }
}
