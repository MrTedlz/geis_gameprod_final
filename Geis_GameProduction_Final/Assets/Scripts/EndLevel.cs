﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //when player collides with end level object and they meet required nut count. increase level and then load new level
        if(collision.CompareTag("Player") && GameController.gc.getNutCount() == 3 && GameController.gc.getLevel() == 1)
        {
            print(GameController.gc.getLevel());
            GameController.gc.addLevel();
            SceneManager.LoadScene(GameController.gc.getLevel().ToString());
        }
        else if (collision.CompareTag("Player") && GameController.gc.getNutCount() == 10 && GameController.gc.getLevel() == 2)
        {
            print(GameController.gc.getLevel());
            GameController.gc.addLevel();
            SceneManager.LoadScene(GameController.gc.getLevel().ToString());
        }
        //on last level load the win page
        else if(collision.CompareTag("Player") && GameController.gc.getNutCount() == 15)
        {
            SceneManager.LoadScene("Win");
        }
    }
}
