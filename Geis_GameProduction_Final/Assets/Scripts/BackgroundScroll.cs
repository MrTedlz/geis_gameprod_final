﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    public float speed;
    private MeshRenderer meshRenderer;

    //initialize renderer
    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    // Update is called once per frame
    void Update()
    {
        float x = Time.deltaTime * speed;

        Vector2 offset = new Vector2(x, 0);
        //create offset for renderer with speed variable
        meshRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
